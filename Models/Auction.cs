﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Collections.ObjectModel;

namespace MvcApplication2.Models
{
    public class Auction
    {
        [Required]
        public long Id { get; set; }

        [Display(Name = "Category")]
        public string Category { get; set; }

        [Required,StringLength(200,MinimumLength=5)]
        [DataType(DataType.Text)]
        [Display(Name = "Title")]
        public string Title { get; set; }
        
        [DataType(DataType.Text)]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [DataType(DataType.ImageUrl)]
        [Display(Name = "Image Url")]
        public string ImageUrl { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = "Start Date")]
        public DateTime StartTime { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = "End Date")]
        public DateTime EndTime { get; set; }

        [DataType(DataType.Currency)]
        [Display(Name = "Start Price")]
        public decimal StartPrice { get; set; }

        [DataType(DataType.Currency)]
        [Display(Name = "End Price")]
        public decimal? CurrentPrice { get; set; }

        public virtual Collection<Bid> Bids { get; private set; }

        public int BidCount
        {
            get { return Bids.Count; }
        }

        public Auction()
        {
            Bids = new Collection<Bid>();
        }
    }
}